# relevant libraries
import rospy
import ros_numpy
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import CompressedImage
from cv_bridge import CvBridge
import numpy
import cv2
import argparse
import message_filters 
from os import path, mkdir
from tf import transformations
from sd_state_msgs.msg import SD_State_Msgs

# class to find depth using LiDAR and transform into camera image frame
class CameraLiDARFusion:
    def __init__(self, extrinsics, camera_intrinsics, distortion_coeffs, dim, scale=1):
        # extrinsinc calibration properties - velodyne to camera
        self.T = extrinsics
        # intrinsic calibration properties
        self.camera_intrinsics = camera_intrinsics
        self.fx=camera_intrinsics[0][0] 
        self.fy=camera_intrinsics[1][1]
        self.cx=camera_intrinsics[0][2]
        self.cy=camera_intrinsics[1][2]
        self.distortion_coeffs = distortion_coeffs

        self.dim = dim
        self.scale = scale
        self.count = 1

    # extrinsic transformation from velodyne to camera frame
    def transformPoint(self, point):
        transformed = numpy.dot(self.T, numpy.append(point,1)) 
        transformed = numpy.delete(transformed,-1,0)
        return transformed
        
    def callback(self, pc2_msg, img_msg, can_msg):
        br = CvBridge()
        img = br.compressed_imgmsg_to_cv2(img_msg, desired_encoding='bgr8') # Convert the compressed message to a new image
        if(img.shape[0]<dim[0] or img.shape[1]<dim[1]): # Skip errors in compressed image message
            print("Skipped small image")
            return
        else:
            print("Depth image " + str(self.count))
        
        undistort_img = cv2.undistort(img, camera_intrinsics, distortion_coeffs) # Undistort image
        undistort_img = cv2.resize(undistort_img, (0,0), fx = self.scale, fy = self.scale) # Resize image by scale factor
        height, width = undistort_img.shape[:2]

        # pointcloud manipulation
        xyz_array = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(pc2_msg)
        depth_array = numpy.zeros((height, width))

        for i in range(0,len(xyz_array)):
            cam_cloud_point = self.transformPoint(xyz_array[i])

            # project from 3d to 2d
            u = int((cam_cloud_point[0]*self.fx/cam_cloud_point[2] + self.cx)*self.scale)
            v = int((cam_cloud_point[1]*self.fy/cam_cloud_point[2] + self.cy)*self.scale)

            if ((u >= 0) and (u < width) and (v >= 0) and (v < height) and cam_cloud_point[2] > 0):
                depth_array[v][u] = cam_cloud_point[2]
        
        # depth image
        depth_data = numpy.array(depth_array * 256, dtype = numpy.uint16) #16 bit depth map

        # save original (unrectified and not scaled) image
        if ORIG_IMAGE:
            cv2.imwrite(IMAGE_FOLDER+"raw/{:05d}.png".format(self.count), img)

        # save depth and rectified/scaled image
        if IMAGE_SAVE:
            cv2.imwrite(IMAGE_FOLDER+"depth/{:05d}.png".format(self.count), depth_data)
            cv2.imwrite(IMAGE_FOLDER+"rectified_images/{:05d}.png".format(self.count), undistort_img)

        # combination image for visual verification 
        if VERIFY: 
            depth_img = numpy.array(depth_array * 255/depth_array.max(), dtype = numpy.uint8) #visual verification 
            depth_map = cv2.applyColorMap(depth_img, cv2.COLORMAP_JET)

            # combine images
            combined = numpy.copy(undistort_img)
            mask = depth_array>0
            combined[mask]=depth_map[mask]           
            cv2.imwrite(IMAGE_FOLDER+"verification/{:05d}.png".format(self.count), combined)
        
        # write depth data to csv
        if CSV_WRITE:
            numpy.savetxt(IMAGE_FOLDER+"csv/{:05d}.csv".format(self.count), depth_array, delimiter=",")

        # save lidar data as binary file (x,y,z,intensity,ring)
        if LIDAR:
            fd = open(IMAGE_FOLDER+"lidar/{:05d}.bin".format(self.count),"w+b")
            fd.write(pc2_msg.data)
            fd.close()

        # velocity
        speed = can_msg.can_data.message_data[10].signal_data[4] # km/h
        ang_vel = [can_msg.can_data.message_data[16].signal_data[0],can_msg.can_data.message_data[16].signal_data[1],can_msg.can_data.message_data[17].signal_data[0]] #deg/s

        with open(IMAGE_FOLDER+'velocity.txt','a') as f:
            f.write('{:.5f},{:.16f},{:.16f},{:.16f}\n'.format(speed,ang_vel[0],ang_vel[1],ang_vel[2]))
        
        with open(IMAGE_FOLDER+'times_lidar.txt','a') as f:
            f.write('{:.9f}\n'.format(pc2_msg.header.stamp.to_sec()))
        with open(IMAGE_FOLDER+'times_cam.txt','a') as f:
            f.write('{:.9f}\n'.format(img_msg.header.stamp.to_sec()))
        with open(IMAGE_FOLDER+'times_can.txt','a') as f:
            f.write('{:.9f}\n'.format(can_msg.header.stamp.to_sec()))
        self.count += 1

    # subscriber
    def subscribe(self):
        
        rospy.loginfo("StreetDrone - Vehicle Camera")
        rospy.loginfo("StreetDrone - Vehicle LiDAR")

        # LiDAR Subscriber
        lidar_sub = message_filters.Subscriber('/points_raw', PointCloud2)

        # Camera Subscriber
        camera_sub = message_filters.Subscriber('/gmsl_camera/port_0/cam_0/image_raw/compressed', CompressedImage)

        # CAN Subscriber
        can_sub = message_filters.Subscriber('/sd_state_msgs', SD_State_Msgs)

        # sync lidar and camera topics 
        ts = message_filters.ApproximateTimeSynchronizer([lidar_sub, camera_sub, can_sub], 10, 0.015, allow_headerless=False) 
        ts.registerCallback(self.callback)

## Main
if __name__ == '__main__':

    # Command Line Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", default=False, type=bool, help="write depth data to csv")
    parser.add_argument("--orig_img", default=False, type=bool, help="save original (unrectified and not scaled) image")
    parser.add_argument("--image", default=True, type=bool, help="save depth and rectified/scaled image")
    parser.add_argument("--verify", default=False, type=bool, help="combination image for visual verification ")
    parser.add_argument("--image_folder", default='data/', type=str, help="path to save image")
    parser.add_argument("--lidar", default=False, type=bool, help="save lidar data in binary files")
    
    args = parser.parse_args()
    
    # Set Flags
    CSV_WRITE = args.csv 
    ORIG_IMAGE = args.orig_img
    IMAGE_SAVE = args.image  
    VERIFY = args.verify
    IMAGE_FOLDER = args.image_folder
    LIDAR = args.lidar

    # setting up storage directories
    if not path.exists(path.dirname(IMAGE_FOLDER)):
        try:
            mkdir(IMAGE_FOLDER)
            if IMAGE_SAVE:
                mkdir(IMAGE_FOLDER + '/depth')
                mkdir(IMAGE_FOLDER + '/rectified_images')
            if VERIFY:
                mkdir(IMAGE_FOLDER + '/verification')
            if ORIG_IMAGE:
                mkdir(IMAGE_FOLDER + '/raw')
            if CSV_WRITE:
                mkdir(IMAGE_FOLDER + '/csv')
            if LIDAR:
                mkdir(IMAGE_FOLDER + '/lidar')
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    # reading camera calibration and transformations
    fs = cv2.FileStorage("streetdrone.yaml", cv2.FILE_STORAGE_READ)
    camera_intrinsics = fs.getNode("CameraMat").mat()
    distortion_coeffs = fs.getNode("DistCoeff").mat()
    extrinsics = fs.getNode("CameraExtrinsicMat").mat()
    dim = [fs.getNode("ImageHeight").real(), fs.getNode("ImageWidth").real()]
    fs.release()

    # ROS Node Initialisation
    rospy.init_node('lidar_camera', anonymous=True)
    clfusion = CameraLiDARFusion(extrinsics, camera_intrinsics, distortion_coeffs, dim, 0.25)
    clfusion.subscribe()

    # Start ROS Loop
    rospy.spin()
