# Depth Labelling
## Purpose
A python script that creates a depth dataset from ROSBAGs with camera, LiDAR and IMU data. It subscribes to `/points_raw`, `/gmsl_camera/port_0/cam_0/image_raw/compressed` and `/sd_state_msgs`, syncing the data, rectifying the camera data and projecting the LiDAR data onto the camera image frame. It is used in conjunction with the team's CAN interface to extract CAN data.

## To use
Camera calibration (intrinsics) and transformations to the LiDAR (extrinsics) are loaded from the file 'streetdrone.yaml', which can easily be updated for the specific vehicle layout and/or cameras. Currently, the script is setup for the centre front camera on the StreetDrone vehicle.

*NOTE: the code currently has a scaling factor on an intrinsic value on line 20, which was added during manual tuning - if calibration is accurate this should be removed*

Run roscore and then the following in terminal:
```console
python2 depth_labelling.py
```

Default Output:
* Timestamps for IMU, LiDAR and camera messages
* IMU - Speed and angular velocity data (can be made optional)

Command Line Options:
* `--image`: save depth and rectified/scaled image [Default = true]
* `--verify`: combination image for visual verification [Default = true]
* `--image_folder`: path to save image (creates directory) [Default = data/]
* `--csv`: write depth data to csv [Default = false]
* `--orig_img`: save original (unrectified and not scaled) image [Default = false]

Start playing a ROSBAG and the chosen outputs will be saved at the specified image directory under relevant sub-folders:
* /depth
* /rectified_images
* /verification
* /raw
* /csv

As this script was written to produce a dataset for neural network training, images are scaled to 25% of the original size. 