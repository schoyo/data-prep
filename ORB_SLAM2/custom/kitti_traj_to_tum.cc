// Used to convert between kitti trajectories to tum format i.e. 3x4 transformation matrix to timestamp, translation, quaternion

#include<iostream>
#include<fstream>
#include<iomanip>

#include<opencv2/core/core.hpp>

#include"Converter.h"

using namespace std;

void LoadTrajectories(const string &strPathToTimestamps, const string &strPathToTraj, vector<double> &vTimestamps, vector<cv::Mat> &vTransformations);
void ConvertSaveTrajectories(const string &filename, vector<double> &vTimestamps, vector<cv::Mat> &vTransformations);

int main(int argc, char **argv)
{
    if(argc != 4)
    {
        cerr << endl << "Usage: ./kitti_traj_to_tum path_to_times path_to_trajectories filename" << endl;
        return 1;
    }

    // Retrieve trajectories and timestamps
    vector<cv::Mat> vTransformations;
    vector<double> vTimestamps;
    LoadTrajectories(string(argv[1]), string(argv[2]), vTimestamps, vTransformations);

    // Convert transformation matrix to translation and quarternion 
    ConvertSaveTrajectories(string(argv[3]), vTimestamps, vTransformations);

    return 0;
}

void LoadTrajectories(const string &strPathToTimestamps, const string &strPathToTraj, vector<double> &vTimestamps, vector<cv::Mat> &vTransformations)
{
    cout << "Loading times ..." << endl;
    ifstream fTimes;
    fTimes.open(strPathToTimestamps.c_str());
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            ss >> t;
            vTimestamps.push_back(t);
        }
    }
    fTimes.close();
    cout << "Times loaded" << endl;

    cout << "Loading trajectories ..." << endl;
    ifstream fTraj;
    fTraj.open(strPathToTraj.c_str());
    while(!fTraj.eof())
    {
        string s;
        getline(fTraj,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            float value;
            cv::Mat T;
            while(ss>>value)
                T.push_back(value);
            T = T.reshape(1,3);
            vTransformations.push_back(T);
        }
    }
    fTraj.close();
    cout << "Trajectories loaded" << endl;
}

void ConvertSaveTrajectories(const string &filename, vector<double> &vTimestamps, vector<cv::Mat> &vTransformations)
{
    cout << "Converting and saving trajectories ..." << endl;
    ofstream f;
    f.open(filename.c_str());
    f << fixed;
    
    for(size_t i=0; i<vTimestamps.size(); i++)
    {
        cv::Mat T = vTransformations[i];
        cv::Mat R = T(cv::Range(0,3),cv::Range(0,3));
        vector<float> q = ORB_SLAM2::Converter::toQuaternion(R);
        cv::Mat t = T.col(3);
        
        f << setprecision(6) << vTimestamps[i] << setprecision(7) << " " << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
          << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;

    }

    f.close();
    cout << "Trajectories saved" << endl;
}