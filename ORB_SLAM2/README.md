# ORB-SLAM2 
All files in this folder are used in conjunction with the official [ORB-SLAM2 repo](https://github.com/raulmur/ORB_SLAM2)

ATE Metrics from [TUM tools](https://vision.in.tum.de/data/datasets/rgbd-dataset/tools#:~:text=Absolute%20Trajectory%20Error%20(ATE),truth%20poses%20using%20the%20timestamps.)
